package com.test.demo.resttemplate.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private WebClient webClient;

    @PostMapping("test")
    public String test() {
        return System.currentTimeMillis() + "";
    }

    @GetMapping("t1")
    public String t1() {
        return restTemplate.postForObject("http://127.0.0.1:8080/test", null, String.class);
    }

    @GetMapping("t2")
    public String t2() {
        Mono<String> response = webClient.post().uri("/test")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.empty())
                .retrieve()
                .bodyToMono(String.class).timeout(Duration.of(10, ChronoUnit.SECONDS));

        String s = response.block();
        System.out.println(s);
        return s;
    }

}
